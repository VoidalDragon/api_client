build:
	docker compose -f local.yml build --no-cache

start:
	docker compose -f local.yml up -d

stop:
	docker compose -f local.yml stop

restart: stop start

shell: 
	docker compose -f local.yml run --rm django bash

migrate: 
	docker compose -f local.yml run --rm django python manage.py migrate

makemigrations:
	docker compose -f local.yml run --rm django python manage.py makemigrations
