import random
import string
import base64
import hashlib
from django.shortcuts import render

from django.views.generic import TemplateView

import requests

RP_URL = "http://ipdjango:8005"

def get_credential():
    client_id = "hOXpzqbfoj0ajJTjM9OoQhVmdgglKpp8VpcnxFF9"
    secret = "SwLIzGbQ2TXSHmENQISZNRKkUONE2y5lRCbNahwBCgUWO3cYyT5TLcL02DdWPwgNP0tsHbWShPtfC0pkXX77NFMevgkt6zAPIK5XeOvuvthVcWpZRjXWOQnsoWCalKMh"

    credential = "{0}:{1}".format(client_id, secret)

    return base64.b64encode(credential.encode("utf-8")).decode()


def get_token():
    credential = get_credential()
    headers = {
        "Authorization": f"Basic {credential}",
        "Content-Type": "application/x-www-form-urlencoded",
        "Cache-Control": "no-cache"
    }
    url = f"{RP_URL}/o/token/"
    print(url, headers)
    return requests.post(url,data={"grant_type":"client_credentials"}, headers=headers).json()['access_token']

def fetch(token):

    headers = {
        "Authorization": f"Bearer {token}",
        "Cache-Control": "no-cache"
    }

    return requests.get("http://secureapi:8003/api/secret/", headers=headers).json()['secret']


class Machine2MachineView(TemplateView):
    template_name = "pages/machine.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books

        token = get_token()
        print(token)


        context["message"] = fetch(token)
        return context
